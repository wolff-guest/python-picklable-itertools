Source: python-picklable-itertools
Maintainer: Fabian Wolff <fabi.wolff@arcor.de>
Section: python
Priority: optional
Build-Depends: debhelper (>= 11),
               dh-python,
               python3,
               python3-setuptools,
               python3-six,
               python3-nose,
               python3-pkg-resources
Standards-Version: 4.1.4
Testsuite: autopkgtest-pkg-python
Homepage: https://github.com/mila-udem/picklable-itertools
Vcs-Git: https://salsa.debian.org/wolff-guest/python-picklable-itertools.git/
Vcs-Browser: https://salsa.debian.org/wolff-guest/python-picklable-itertools

Package: python3-picklable-itertools
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-pkg-resources
Description: picklable reimplementation of Python's itertools for Python 3
 Picklable-itertools is a reimplementation of the Python standard
 library's "itertools", in Python, using picklable iterator objects.
 .
 It is intended as a drop-in replacement in cases where functionality
 from itertools is needed, but serializability has to be maintained.
 .
 This package provides the picklable_itertools Python module for
 Python 3.x.
